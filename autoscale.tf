resource "aws_cloudwatch_metric_alarm" "web_server_cpu_alarm" {
  alarm_name     = "web-server-cpu-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "300"
  statistic = "Average"
  threshold = "80"
  alarm_description = "Alarm when the CPU utilization exceeds 80% for the web server"
  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.web_server_asg.name
 }
    alarm_actions     = [aws_autoscaling_policy.web.arn]
}
resource "aws_cloudwatch_metric_alarm" "nlb_hosts" {
  alarm_name          = "nlb_dead_hosts"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = 1
  metric_name         = "HealthyHostCount"
  namespace           = "AWS/ApplicationELB"
  period              = 60
  statistic           = "Average"
  threshold           = var.servers_count
  alarm_description   = "Number of healthy nodes in Target Group"
  actions_enabled     = "true"
  alarm_actions       = [aws_autoscaling_policy.test_lb.arn]
  dimensions = {
    TargetGroup  = aws_lb_target_group.lb1.arn_suffix
    LoadBalancer = aws_lb.lb1.arn_suffix
  }
}

resource "aws_launch_configuration" "web_server" {
  name_prefix     = "web_server"
  image_id        = data.aws_ami.ubuntu.id
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.access_sg.id]
  key_name = "web"
  user_data = file("templates/nginx.sh")

  lifecycle {
    create_before_destroy = true
  }
}
# Create an Auto Scaling group for web servers
resource "aws_autoscaling_group" "web_server_asg" {
  name                    = "web-server-asg"
  launch_configuration     = aws_launch_configuration.web_server.name
  min_size                = 2
  max_size                = 4
  health_check_type       = "ELB"
  health_check_grace_period = 300
  vpc_zone_identifier     =  module.vpc.public_subnets 
  target_group_arns    =  [aws_lb_target_group.lb1.arn]


}
resource "aws_autoscaling_policy" "web" {
 # count = 2
  name = "scale_up_policy"
  scaling_adjustment     = 1
  policy_type            = "SimpleScaling"
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.web_server_asg.name
}
resource "aws_autoscaling_policy" "test_lb" {
   name = "test_lb"
   autoscaling_group_name  = aws_autoscaling_group.web_server_asg.name
  policy_type            = "StepScaling"
  adjustment_type = "ChangeInCapacity"
  step_adjustment {
    scaling_adjustment          = 1 
metric_interval_upper_bound  = 0
  }
}
