# Terraform AWS Infrastructure Repository

This repository contains Terraform code to provision and manage AWS infrastructure for a web application. The code creates EC2 instances, an RDS database, security groups, and sets up CloudWatch alarms for monitoring. Below, we provide a high-level overview of the code and its AWS resources.

## AWS Resources and Their Dependencies

### Virtual Private Cloud (VPC) and Networking

- **Data Source for Available Availability Zones**:
  - Retrieves available AWS availability zones for creating resources.


- **VPC Module** (`module "vpc"`):
  - Deploys a VPC with public and private subnets, DNS settings, and other configurations.
  - Dependencies: Availability zones data source.

- **Security Group** (`resource "aws_security_group" "access_sg"`):
  - Manages security group rules for EC2 instances.
  - Depends on the VPC module.

### CloudWatch Alarms for EC2 Instances

- **CloudWatch Metric Alarms for CPU and Memory**:
  - Monitors CPU and memory usage on EC2 instances.
  - Dependencies: EC2 instances.

### CloudWatch Alarms for RDS Database

- **CloudWatch Metric Alarms for RDS CPU and Memory**:
  - Monitors CPU and memory usage on the RDS database.
  - Dependencies: RDS database instance.

### Simple Notification Service (SNS)

- **SNS Topic for Alerts** (`resource "aws_sns_topic" "alerts_topic"`):
  - Creates an SNS topic for receiving alerts.
  - No direct dependencies.

- **SNS Subscription for Email Notifications** (`resource "aws_sns_topic_subscription" "email_subscription"`):
  - Subscribes to the SNS topic for email notifications.
  - Depends on the SNS topic.

### EC2 Instances and Auto Scaling

- **Launch Configuration for EC2 Instances** (`resource "aws_launch_configuration" "web_server"`):
  - Configures the launch settings for EC2 instances.
  - Depends on the VPC and security group.

- **Auto Scaling Group for EC2 Instances** (`resource "aws_autoscaling_group" "web_server_asg"`):
  - Manages the Auto Scaling group for EC2 instances.
  - Depends on the launch configuration and VPC.

- **Auto Scaling Policies** (`resource "aws_autoscaling_policy" "web"`):
  - Configures scaling policies for the Auto Scaling group.
  - Depends on the Auto Scaling group.

### RDS Database

- **DB Subnet Group** (`resource "aws_db_subnet_group" "db"`):
  - Creates a subnet group for the RDS database.
  - Depends on the VPC.

- **RDS Database Instance** (`resource "aws_db_instance" "database"`):
  - Provisions an RDS database instance.
  - Depends on the DB subnet group, VPC, and security group.

## Variables

- `aws_region`: AWS region for deployment.
- `db_user` and `db_password`: Database credentials.
- `AWS_SECRET_ACCESS_KEY`: Provision via variable.
- `AWS_ACCESS_KEY_ID`: Provision via variable.



## Output

- The repository provides outputs for RDS database endpoint and instance address.

## Usage

1. Set your AWS region, database credentials, and other variables in the code.
2. Wait for pipeline to finish.
3. Run `deploy` stage to create and provision the AWS resources.
4. Monitor your infrastructure through CloudWatch alarms and receive alerts via SNS.

