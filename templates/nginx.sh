#!/bin/bash
set -x
exec > >(tee /var/log/tf-user-data.log|logger -t user-data ) 2>&1

logger() {
  DT=$(date '+%Y/%m/%d %H:%M:%S')
  echo "$DT $0: $1"
}

logger "Running"

logger "Setting timezone to UTC"
sudo timedatectl set-timezone UTC


# Update the package manager
sudo apt-get -qq -y update
# Install Nginx
sudo apt-get install -y nginx
# Start Nginx
sudo systemctl start nginx
# Enable Nginx to start on boot
sudo systemctl enable nginx
file="/var/www/html/index.nginx-debian.html"
sed -i 's/Welcome to nginx!/Hello Endava/g' ${file}
logger "Complete"
