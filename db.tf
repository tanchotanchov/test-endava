# Create RDS database instance
resource "aws_db_subnet_group" "db" {
  name      = "db"
  subnet_ids     =  module.vpc.private_subnets
}
resource "aws_db_instance" "database" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  db_name              = "bazadanni"
  username             = var.db_user
  password             = var.db_password
  parameter_group_name = "default.mysql5.7"
  db_subnet_group_name   = aws_db_subnet_group.db.name
  skip_final_snapshot  = true
  vpc_security_group_ids = [aws_security_group.access_sg.id]
}
