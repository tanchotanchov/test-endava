resource "aws_cloudwatch_metric_alarm" "db_cpu_alarm" {
  alarm_name          = "DBHighCPUAlarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/RDS"
  period             = "60"
  statistic           = "Average"
  threshold           = "80"
  alarm_description   = "CPU usage on RDS instance is above 80%"
  alarm_actions       = [aws_sns_topic.alerts_topic.arn]

 dimensions = {
   DBInstanceIdentifier = aws_db_instance.database.id
 }
}

resource "aws_cloudwatch_metric_alarm" "db_memory_alarm" {
  alarm_name          = "DBHighMemoryAlarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name         = "FreeableMemory"
  namespace           = "AWS/RDS"
  period             = "60"
  statistic           = "Average"
  threshold           = "1000000000"
  alarm_description   = "Memory usage on RDS instance is above 80%"
  alarm_actions       = [aws_sns_topic.alerts_topic.arn]

  dimensions = {
    DBInstanceIdentifier = aws_db_instance.database.id
  }
}

resource "aws_sns_topic" "alerts_topic" {
  name = "Alerts"
}
resource "aws_cloudwatch_metric_alarm" "ec2_cpu_alarm" {
  alarm_name          = "WebSRVHighCPUAlarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period             = "60"
  statistic           = "Average"
  threshold           = "80"
  alarm_description   = "CPU usage on EC2 instance is above 80%"
  alarm_actions       = [aws_sns_topic.alerts_topic.arn]

  dimensions = {
     AutoScalingGroupName = aws_autoscaling_group.web_server_asg.name
  }
}

resource "aws_sns_topic_subscription" "email_subscription" {
  topic_arn = aws_sns_topic.alerts_topic.arn
  protocol  = "email"
  endpoint  = "tdt@abv.bg" 
}
