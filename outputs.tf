# Output the Load Balancer DNS name
output "load_balancer_dns" {
  value = aws_lb.lb1.dns_name
}
output "rds_endpoint" {
  value = aws_db_instance.database.endpoint
}
output "instance_address" {
  value       = aws_db_instance.database.address
}
