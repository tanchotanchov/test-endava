variable "aws_region" {
  type        = string
  default     = "us-east-1"
}
variable "db_user" {
  type = string
  default = "admin"
}

variable "db_password" {
  type = string
  default = "Mn##g0Par0la"
}
variable "servers_count" {
  description = "Number of healthy servers in the target group"
  type        = number
  default     = 3
}
