terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}
# Define your AWS provider
provider "aws" {
  region = var.aws_region 
}
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical account ID for Ubuntu images
}

resource "aws_key_pair" "web" {
  key_name   = "web"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCiB9zYqZjV9t8jIyscthCP15g0rZVuL0Om68mrgsiYHhYee4yr6Npeu30+tDnx8g1pUA0BwAQjobN390Sw0BxjBN+uyv0C8f21UlsQVtjKuWPSCEtW9ny0+vmDzcpd1/rOLG3pSVLp2res7+94R+4YY0c8X660k8crtzI0HaiNhtmgEomIXOXndQfDC6z/oQEcc+wciGakLTQjht7CzpBemafHTU/bUZHSr8b+bx23/YxkdkXim8wi+jADf4kn6pIL/A63+y2b5gBgpzuXYghNmqnD5s4okxdvTUodjyDY7U6LhUTJPMcX9u01HqQs6jw4XrMSa6FCilZZqr4uR/RwU0BQTk5MfeLceedhyKxfgOPvfV4AWNslK5G9n0yycz1yQiRfgcz0QMInrIscWkeGoAW30m7I8opIBcyfg8zHPikRVGI50aitKuX4+87Ja7JRJUY+jZsFdlbZLJs8ZzGUTnza7gJOyJsg8FixNtKMy9S3J1O0CcSNG94QV5Pb2mc= root@my-ansible"
}



# Create an Application Load Balancer
resource "aws_lb" "lb1" {
  name               = "lb1-test"
  internal           = false
  load_balancer_type = "application"
  subnets            = module.vpc.public_subnets
  security_groups     = [aws_security_group.access_sg.id]
}
# Define the listener for the Load Balancer
resource "aws_lb_listener" "lb1" {
  load_balancer_arn = aws_lb.lb1.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lb1.arn
  }
}
resource "aws_lb_target_group" "lb1" {
  name     = "web-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = module.vpc.vpc_id 

}

